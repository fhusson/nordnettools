# NordnetTools

A simple command line to get information from my Nordnet Box.

Nordnet is a french Internet Provider.

<https://www.nordnet.com/>

## Usage

### To get help

`NordnetTools.exe --help`

### To get all available information

`NordnetTools.exe`

### To get you internet IP

`NordnetTools.exe -i wanipv4address`
