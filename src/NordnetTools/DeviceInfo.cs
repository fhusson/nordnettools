﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NordnetTools
{
    public class DeviceInfo : Dictionary<string, DeviceInfoItem>
    {
        const string patternTemplateTableTd = "<td class='hd'>{0}</td>.*?<td>(.*?)</td>";
        const string patternTemplateJavascriptVar = "var {0} = '(.*?)';";

        public DeviceInfo()
        {
            AddItem("boardid", "Board ID", patternTemplateTableTd);
            AddItem("hardwareversion", "HardWare Version", patternTemplateTableTd);
            AddItem("serialnumber", "Serial Number", patternTemplateTableTd);
            AddItem("macaddress", "Mac Address", patternTemplateTableTd);
            AddItem("buildtimesteamp", "Build Timestamp", patternTemplateTableTd);
            AddItem("softwareversion", "Software Version", patternTemplateTableTd);
            AddItem("bootloaderversion", "Bootloader (CFE) Version", patternTemplateTableTd, "Bootloader \\(CFE\\) Version:");
            AddItem("wirelessdriverversion", "Wireless Driver Version", patternTemplateTableTd);
            AddItem("uptime", "Uptime", patternTemplateTableTd);
            AddItem("configid", "ConfigId", patternTemplateTableTd);
            AddItem("lanipv4address", "LAN IPv4 Address", patternTemplateTableTd);
            AddItem("wanipv4address", "WAN IPv4 Address", patternTemplateTableTd);
            AddItem("defaultgateway", "Default Gateway", patternTemplateJavascriptVar, "dfltGw");
            AddItem("primarydnsserver", "Primary DNS Server", patternTemplateTableTd);
            AddItem("secondarydnsserver", "Secondary DNS Server", patternTemplateTableTd);
        }

        private void AddItem(string id, string name, string patternTemplate)
        {
            var patternParam = $"{name}:";
            var pattern = string.Format(patternTemplate, patternParam);
            var item = new DeviceInfoItem { Id = id, Name = name, Pattern = pattern };
            this.Add(item.Id, item);
        }
        private void AddItem(string id, string name, string patternTemplate, string patternParam)
        {
            var pattern = string.Format(patternTemplate, patternParam);
            var item = new DeviceInfoItem { Id = id, Name = name, Pattern = pattern };
            this.Add(item.Id, item);
        }
    }
}