﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NordnetTools
{
    public class DeviceInfoItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Pattern { get; set; }
        public string Value { get; set; }
    }
}
