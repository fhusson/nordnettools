﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NordnetTools
{
    class BoxWebsite
    {
        public const string DefaultBaseUrl = @"http://192.168.5.1";
        public const string DefaultLogin = "nordnet";
        public const string DefaultPassword = DefaultLogin;

        public string BaseUrl { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public BoxWebsite()
        {
            this.BaseUrl = DefaultBaseUrl;
            this.Login = DefaultLogin;
            this.Password = DefaultPassword;
        }

        public async Task<DeviceInfo> GetDeviceInfo()
        {
            var client = new HttpClient()
            {
                BaseAddress = new Uri(this.BaseUrl)
            };

            var request = new HttpRequestMessage(HttpMethod.Get, DeviceInfoPage.Name);
            var authenticationString = $"{this.Login}:{this.Password}";
            var base64EncodedAuthenticationString = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(authenticationString));
            request.Headers.Add("Authorization", "Basic " + base64EncodedAuthenticationString);

            var response = await client.SendAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var content = await response.Content.ReadAsStringAsync();

                return DeviceInfoPage.Parse(content);
            }
            else
            {
                var message = $"Error trying to get page '{request.RequestUri}', status code is {response.StatusCode}";
                throw new System.Net.WebException(message);
            }
        }
    }
}
