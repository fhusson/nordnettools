﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NordnetTools
{
    public class DeviceInfoPage
    {
        public const string Name = @"info.html";
        private static string ParseValue(string content, string pattern)
        {
            var match = Regex.Match(content, pattern, RegexOptions.Singleline);
            var value = match.Groups[1].Value;
            value = value.Replace("&nbsp;", " ");
            value = value.Trim();
            return value;
        }

        public static DeviceInfo Parse(string content)
        {
            var deviceInfo = new DeviceInfo();

            foreach (var item in deviceInfo.Values)
            {
                item.Value = ParseValue(content, item.Pattern);
            }

            return deviceInfo;
        }
    }
}
