﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.CommandLine;
using System.CommandLine.Invocation;

namespace NordnetTools
{
    class Program
    {
        static int Main(string[] args)
        {
            // Create a root command with some options
            var rootCommand = new RootCommand
            {
                new Option<string>(
                    new string[2] { "--url", "-u" },
                    getDefaultValue: () => BoxWebsite.DefaultBaseUrl,
                    description: "Url of the Box"),
                new Option<string>(
                    new string[2] { "--login", "-l" },
                    getDefaultValue: () => BoxWebsite.DefaultLogin,
                    description: "Login"),
                new Option<string>(
                    new string[2] { "--password", "-p" },
                    getDefaultValue: () => BoxWebsite.DefaultPassword,
                    description: "Password"),
                new Option<string>(
                    new string[2] { "--id", "-i" },
                    description: "Info Id"),
                new Option<bool>(
                    "--get-all-ids",
                    description: "Display all available info Ids"),
            };
            rootCommand.Description = "A command line app to retrieve information from my Nordnet Box";

            // Note that the parameters of the handler method are matched according to the names of the options
            rootCommand.Handler = CommandHandler.Create<string, string, string, string, bool>((url, login, password, id, getAllIds) =>
            {
                if (getAllIds)
                {
                    DisplayAllIds();
                }
                else
                {
                    if (!string.IsNullOrEmpty(id))
                        DisplayDeviceInfo(url, login, password, id);
                    else
                        DisplayAllDeviceInfo(url, login, password);
                }
            });

            // Parse the incoming args and invoke the handler
            return rootCommand.InvokeAsync(args).Result;
        }

        public static void DisplayAllIds()
        {
            var emptyDeviceInfo = new DeviceInfo();
            foreach (var item in emptyDeviceInfo.Values)
            {
                Console.WriteLine($"{item.Id} : Display {item.Name}");
            }
        }

        public static void DisplayAllDeviceInfo(string url, string login, string password)
        {
            var website = new BoxWebsite()
            {
                BaseUrl = url,
                Login = login,
                Password = password
            };

            var deviceInfo = website.GetDeviceInfo().Result;
            foreach (var item in deviceInfo.Values)
            {
                Console.WriteLine($"{item.Name}: {item.Value}");
            }
        }

        public static void DisplayDeviceInfo(string url, string login, string password, string infoId)
        {
            var website = new BoxWebsite()
            {
                BaseUrl = url,
                Login = login,
                Password = password
            };

            var deviceInfo = website.GetDeviceInfo().Result;
            var item = deviceInfo[infoId];
            Console.WriteLine($"{item.Value}");
        }
    }
}
