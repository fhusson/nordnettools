using System;
using Xunit;
using NordnetTools;
using System.IO;

namespace NordnetTools.Tests
{
    public class DeviceInfoPageUnitTest
    {
        [Fact]
        public void GivenSample1_WhenParse_ThenReturnInfo()
        {
            string content = File.ReadAllText($"sample1_{DeviceInfoPage.Name}");
            DeviceInfo deviceInfo = DeviceInfoPage.Parse(content);

            // Assert
            Assert.Equal("F@ST5350V2", deviceInfo["boardid"].Value);
            Assert.Equal("F5350V2_1.00", deviceInfo["hardwareversion"].Value);
            Assert.Equal("N1234567U123456", deviceInfo["serialnumber"].Value);
            Assert.Equal("ab:cd:ef:gh:ij:kl", deviceInfo["macaddress"].Value);
            Assert.Equal("200107_1708", deviceInfo["buildtimesteamp"].Value);
            Assert.Equal("9.29.24.9_F5350V2_Nordnet", deviceInfo["softwareversion"].Value);
            Assert.Equal("9.29.", deviceInfo["bootloaderversion"].Value);
            Assert.Equal("7.14 RC89.3303", deviceInfo["wirelessdriverversion"].Value);
            Assert.Equal("149D 11H 44M 5S", deviceInfo["uptime"].Value);
            Assert.Equal("F5350V2_Nordnet_7.conf", deviceInfo["configid"].Value);
            Assert.Equal("192.168.5.1", deviceInfo["lanipv4address"].Value);
            Assert.Equal("12.345.678.42", deviceInfo["wanipv4address"].Value);
            Assert.Equal("12.345.678.254", deviceInfo["defaultgateway"].Value);
            Assert.Equal("1.1.1.1", deviceInfo["primarydnsserver"].Value);
            Assert.Equal("1.0.0.1", deviceInfo["secondarydnsserver"].Value);
        }
    }
}
